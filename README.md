# README #

Author: Karri Pyykkö

### VIDEO LINK ###
https://youtu.be/IhO7bEBeq6g

### What is this repository for? ###

This a repository containing all the course work for SDS: Mobile -course

* __Introduction__ Part 1 project
* __CoreElements__ Part 2 project
* __ListLayoutsImages__ Part 3 project
* __UnicodeLookup__ Final project
* __LearningDiary_Karri_Pyykko.pdf__ Learning diary

### How do I get set up? ###

The project seems to have issues with recognizing the build.gradle, as such it's recommended to download the project files instead of importing through Android Studio VCS:

* Download the repository
* Extract files
* Android studio: File > Open