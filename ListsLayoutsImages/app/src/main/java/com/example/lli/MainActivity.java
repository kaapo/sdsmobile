package com.example.lli;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView list;
    String[] items, prices, descriptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources res = getResources();
        list = findViewById(R.id.recyclerView1);
        items = res.getStringArray(R.array.list_items);
        prices = res.getStringArray(R.array.list_prices);
        descriptions = res.getStringArray(R.array.list_descriptions);

        Log.d("asd", String.valueOf(items.length));

        RecyclerView.Adapter adapter = new recyclerViewAdapter(this, items, prices, descriptions);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);

        //Log.d("test", String.valueOf(list.getAdapter().getItemCount()));
    }
}