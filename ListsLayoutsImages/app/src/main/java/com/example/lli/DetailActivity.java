package com.example.lli;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

public class DetailActivity extends AppCompatActivity {

    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        img = findViewById(R.id.imageView);

        int idx = getIntent().getIntExtra("IMAGE_INDEX", -1);

        if (getImg(idx) != -1){
            img.setImageResource(getImg(idx));
        }
    }

    private int getImg(int idx){
        switch (idx){
            case 0:
                return R.drawable.peach;
            case 1:
                return R.drawable.tomato;
            case 2:
                return R.drawable.squash;
            default:
                return -1;
        }
    }
}