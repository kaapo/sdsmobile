package com.example.lli;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class recyclerViewAdapter extends RecyclerView.Adapter<recyclerViewAdapter.ViewHolder> {

    private String[] mNames, mPrices, mDesc;
    private Context mContext;
    private LayoutInflater mInflater;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemName;
        public TextView itemPrice;
        public TextView itemDescription;

        public ViewHolder(View itemView){
            super(itemView);

            itemName = itemView.findViewById(R.id.ListTextView);
            itemPrice = itemView.findViewById(R.id.textViewPrice);
            itemDescription = itemView.findViewById(R.id.textViewDesc);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getBindingAdapterPosition();
                    Intent intent = new Intent(mContext, DetailActivity.class);
                    intent.putExtra("IMAGE_INDEX", position);
                    mContext.startActivity(intent);
                }
            });
        }
    }

    public recyclerViewAdapter(Context context, String[] names, String[] prices, String[] desc){
        mNames = names;
        mPrices = prices;
        mDesc = desc;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_detail, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder) holder;

        vh.itemName.setText(mNames[position]);
        vh.itemPrice.setText(mPrices[position]);
        vh.itemDescription.setText(mDesc[position]);
        //Log.d("asd2", mNames[position]);
    }

    @Override
    public int getItemCount() {
        return mNames.length;
    }
}
