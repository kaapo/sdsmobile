package com.example.coreelements;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.Console;

public class MainActivity extends AppCompatActivity {

    Button secondActivity, googleActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        secondActivity = (Button) findViewById(R.id.secondActivityButton);
        googleActivity = (Button) findViewById(R.id.googleButton);

        secondActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("test", getPackageName());

                Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
                intent.putExtra("com.example.coreelements.MainActivityExtra", "yo");
                startActivity(intent);
            }
        });

        googleActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://www.google.com";
                Uri address = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, address);

                if (intent.resolveActivity(getPackageManager()) != null){
                    startActivity(intent);
                }
            }
        });
    }
}