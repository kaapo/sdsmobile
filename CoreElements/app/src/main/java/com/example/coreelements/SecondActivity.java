package com.example.coreelements;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        text = findViewById(R.id.textView1);

        if (getIntent().hasExtra("com.example.coreelements.MainActivityExtra")){
            text.setText(getIntent().getExtras().getString("com.example.coreelements.MainActivityExtra"));
        }
    }
}