package com.example.introduction_part1;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    EditText num1, num2;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num1 = findViewById(R.id.firstNumEditText);
        num2 = findViewById(R.id.firstNumEditText2);
        result = findViewById(R.id.resultTextView);
    }

    public void addition(View v){
        double sum;

        sum = Double.parseDouble(num1.getText().toString()) + Double.parseDouble(num2.getText().toString());
        result.setText(String.valueOf(sum));
    }
}